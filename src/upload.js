const express = require('express'),
    path = require('path'),
    cors = require('cors'),
    multer = require('multer'),
    bodyParser = require('body-parser');

// File upload settings  
const storagePATH = '/home/robert.poenaru/elk/NODE-UPLOAD/public/uploads/';
const PATH = '/home/robert.poenaru/elk/NODE-UPLOAD/public/';

let storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, storagePATH);
    },
    filename: (req, file, cb) => {
        cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname));
    }
});

let upload = multer({
    storage: storage
});


// Express settings
//app render
const app = express();
app.set('view engine', 'ejs');
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: false
}));

app.get('/api', function(req, res) {
    res.end('File catcher');
});
app.get('/', (req, res) => {
    res.render(path.join(PATH, 'index.ejs'));
});

// POST File
app.post('/api/upload', upload.single('upfile'), function(req, res) {
    if (!req.file) {
        console.log("No file is available!");
        return res.send({
            success: false
        });

    } else {
        console.log('File is available!');
        return res.send({
            success: true
        })
    }
});

// Create PORT
const PORT = process.env.PORT || 8082;
const server = app.listen(PORT, () => {
    console.log('Connected to port ' + PORT)
})

/* // Find 404 and hand over to error handler
app.use((req, res, next) => {
    next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
    console.error(err.message);
    if (!err.statusCode) err.statusCode = 500;
    res.status(err.statusCode).send(err.message);
}); */