const express = require('express')
const multer = require('multer');
const path = require('path');
var bodyParser = require('body-parser');
var engine = require('consolidate');
var pug = require('pug');
var ejs = require('ejs');
const fs = require('fs');
const app = express()
const PORT = 8082

const testFolder = '/home/robert.poenaru/elk/NODE-UPLOAD/public/uploads/';
//get folder content

//set storage engine with multer

// SET STORAGE
var storage = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, '/home/robert.poenaru/elk/NODE-UPLOAD/public/uploads/')
    },
    filename: function(req, file, cb) {
        cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname));
    }
});

var upload = multer({ storage: storage });

//RENDER SYSTEM WITH PUG
/* app.set('views', __dirname + '/views');
app.set('view engine', 'pug')
app.use(bodyParser.urlencoded({ extended: true }));
*/

//ejs render
app.set('view engine', 'ejs');

//static folder
app.use(express.static('/home/robert.poenaru/elk/NODE-UPLOAD/public/'));
//app.get('/', (req, res) => res.sendFile(path.join('/home/robert.poenaru/elk/NODE-UPLOAD/html', '/', 'index.html')));

app.get('/', (req, res) => res.render('/home/robert.poenaru/elk/NODE-UPLOAD/views/index'));
//pug
/* app.get('/user/:id', (req, res) => {
    var title = req.params.id;
    res.render(path.join('/home/robert.poenaru/elk/NODE-UPLOAD/html', '/', 'index.pug'), { title: `Hey there ${title}`, message: `It's all good ${title}`, message2: `Hey there ${title}`, para: 'This is a paragraph', para2: 'This is another paragraph' });
})
 */

app.post('/delete', (req, res) => {
    const file = '/home/robert.poenaru/elk/NODE-UPLOAD/public/uploads/upfile-1574081723671.doc';
    fs.unlink(file, err => {
        if (err) {
            console.log(err);
            return
        }
        console.log('File deleted ok');
    });
    res.redirect('list');
});

app.get('/list', (req, res) => {
    var filelist = [];

    function readfiles() {
        return new Promise((resolve) => {
            fs.readdir(testFolder, (err, files) => {
                var count = 0;
                files.forEach(file => {
                    count += 1;
                    //console.log(file);
                    filelist.push({ id: count, FileName: file });
                });
                if (count != 0) {
                    resolve();
                } else {
                    reject('Empy folder');
                }

            });
            /*   if (count > 0) {
                  resolve();
              } else {
                  reject('Empty folder');
              } */
        })
    }
    /*    async function init() {
           try {
               const x = await readfiles();
               console.log(filelist);
           } catch (error) {
               console.log(error);
           }

       }
       init();
    */

    function generatefile(x) {
        return new Promise((resolve, reject) => {
            var stream = fs.createWriteStream(path.join('/home/robert.poenaru/elk/NODE-UPLOAD/public/', '/', 'myfiles.ejs'));
            stream.once('open', (fd) => {
                var count = 0;
                stream.write(
                    '<!DOCTYPE html><html lang="en"><head><meta charset="UTF-8"><meta name="viewport" content="width=device-width, initial-scale=1.0"><meta http-equiv="X-UA-Compatible" content="ie=edge"><title>NODE-JS-UPLOAD</title><link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css"><style>th, td { text-align: center;}</style></head><body><script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script><script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script><div class="container"><h1>File-upload</h1></div>'
                );
                stream.write('<div class="row"><div class="col s12 m5"><div class="card-panel teal"><span class="white-text">');
                stream.write('<table><tr><th>Index</th><th>File Name</th></tr>');
                stream.write('<div class="container">');
                x.forEach(element => {
                    count += 1;
                    // console.log(element.id + ' ' + element.FileName);
                    stream.write('<tr>');
                    stream.write(`<td><p>${element.id}</p></td>`);
                    stream.write(`<td><p>${element.FileName}</p></td>`);
                    stream.write('</tr>');
                })
                stream.write('</div>');
                stream.write('</table>');
                stream.write('</span></div></div></div>');
                stream.write('</body></html>');
                stream.end();
                if (count === x.length) {
                    resolve();
                } else {
                    reject('Empty folder');
                }
            });
        })
    };

    async function getfile() {
        try {
            await readfiles();
            await generatefile(filelist);
            var yy = fs.readFile(path.join('/home/robert.poenaru/elk/NODE-UPLOAD/public/', 'myfiles.ejs'), 'utf-8', () => { res.render(path.join('/home/robert.poenaru/elk/NODE-UPLOAD/public/', 'myfiles.ejs')); });
            console.log(yy);

        } catch (error) {
            console.log(error);
            res.send('Not good');
        }
    }

    getfile();

    //promise version without async await
    //hello().then(res.render(path.join('/home/robert.poenaru/elk/NODE-UPLOAD/public', '/', 'myfiles.ejs')));
    // res.send('test');
});

app.get('/list2', (req, res) => {
    res.sendFile(path.join('/home/robert.poenaru/elk/NODE-UPLOAD/public/', 'myfiles.html'));
})
app.get('/clean', (req, res) => {
    res.render(path.join(__dirname, '../', '/public/', 'cleaner.ejs'));
})
app.post('/upload', upload.single('upfile'), (req, res, next) => {
    const file = req.file;
    if (!file) {
        const error = new Error('Please upload a file');
        error.httpStatusCode = 400;
        return next(error);
    }
    res.send(file);
});
/* 
app.post('/upload', (req, res) => {
    upload(req, res, (err) => {
        if (err) {
            res.render('index', { msg: err });
        } else {
            console.log(req.file);
            res.send('post ok');
        }
    });
}) */


app.listen(PORT, () => console.log(`Example app listening on port ${PORT}!`))